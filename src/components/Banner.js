// import the button, row and column components from react bootstrap
// import Button from 'react-bootstrap/Button'
// // Bootstrap grid system components (row & col)
// import Row from 'react-bootstrap/Row'
// import Col from 'react-bootstrap/Col'

/*export default function Banner(){
	return(
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>
	)
}*/


// Answer Key (404 Activity)
import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({data}){
	console.log(data)
	const {title, content, destination, label} = data;
	return(
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn btn-primary" to={destination}>{label}</Link>
			</Col>
		</Row>
	)
}
