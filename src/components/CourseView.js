import { useState, useEffect, useContext } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView() {
	// useParams hook allows retrieval of courseId passed via url
	const { courseId }= useParams()

	// useContext to obtain the userId to enroll in a course; general use-share values of different components
	const { user } = useContext(UserContext);
	
	// useHistory to redirect to other pages
	const history = useHistory();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	// enroll a function to enroll a user to a specific course
	const enroll = (courseId) => {
		fetch('http://localhost:4000/api/users/enroll', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			if(data === true){
				Swal.fire({
						title: "Successfully Enrolled",
						icon: "success",
						text: "You have successfully enrolled for this course"
				})
				history.push('/courses');
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}
	// useEffect hook checks if the courseId is retrieved properly
	useEffect(()=>{
		console.log(courseId)

		fetch(`http://localhost:4000/api/courses/${courseId}`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [ courseId ]);


	return(
		<Container className='mt-5'>
			<Row>
				<Col lg={ {span: 6, offset: 3} }>
					<Card className="mb-2">
						  <Card.Body className='text-center'>
							    <Card.Title>{name}</Card.Title>
							    <Card.Text>Description:</Card.Text>
							    <Card.Text>{description}</Card.Text>
							    <Card.Text>Price:</Card.Text>
							    <Card.Text>Php {price}</Card.Text>
							    <Card.Subtitle>Class Schedule</Card.Subtitle>
							    <Card.Text>8:00 am - 5:00pm</Card.Text>
							    {
							    	user.id !== null ?
										<Button variant="primary" onClick={ () => enroll(courseId) }>Enroll</Button>
									:
										<Link className="btn btn-danger btn-block" to="/login">Login to Enroll</Link>
							    }
						  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}