// import state and effect hooks from react
// import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {Button, Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	// checks if the data was successfully passed
	// console.log (props);
	// console.log(typeof props)
	
	const { _id, name, description, price} = courseProp

	// use state hook in this componene to stor its state
		// states are used to keep track of information related to individual components
			/*Syntax:
					const [getter, setter] = useState(initialGetterValue)
			*/
/*	const [count, setCount] = useState(0);
	const [seats, setSeatCount] = useState(30);
	const [isOpen, setIsOpen] = useState(false)

	console.log(useState(0));*/
	// function to keep track of the enrollees of a course
/*	function enroll(){
			setCount(count + 1);
			console.log('Enrollees: ' + count)
			setSeatCount(seats - 1);
			console.log('Seats: ' + seats)
	}
*/

	// Define  a useEffect hook to have the CourseCard component perform a certain task after every DOM update
/*	useEffect(()=>{
		if (seats===0){
			setIsOpen(true);
		}
	}, [seats])*/


	return(	
				<Card className="cardHighlights mb-2">
					  <Card.Body>
						    <Card.Title>{name}</Card.Title>
						    <Card.Text >Description:</Card.Text>
						    <Card.Text >{description}</Card.Text>
						    <Card.Text >Price:</Card.Text>
						    <Card.Text >Php {price}</Card.Text>
							<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
					  </Card.Body>
				</Card>
	)
}