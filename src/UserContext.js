import React from 'react';

// create a Context object
const UserContext = React.createContext();

// The "Provider" component allows other components to consume.use the context object and supply the necessary information needed to the context objects
export const UserProvider = UserContext.Provider;

export default UserContext;