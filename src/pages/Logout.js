import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';


export default function Logout() {
	// localStorage.clear();

	const {unsetUser, setUser} = useContext(UserContext);

	// clears the localStorage for the user's information
	unsetUser();

	useEffect(() =>{
		// sets the user state back to its original value
		setUser({id: null})
	})
	
	// redirect back to login
	return(
		<Redirect to='/login' />
	)
}