import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(props) {
		const {user, setUser} = useContext(UserContext);
		const history = useHistory();

		//  state hooks to store the values of the input fields
		const [email, setEmail] = useState('')
		const [firstName, setFirstName] = useState('')
		const [lastName, setLastName] = useState('')
		const [age, setAge] = useState('')
		const [gender,setGender] = useState('')
		const [ mobileNo, setMobileNo] = useState('')
		const [ password1, setPassword1] = useState('')
		const [password2, setPassword2] = useState('')
		// state to determine whether the submit button is enabled or not
		const [isActive, setIsActive] = useState(false)

		// checks if the values are successfully binded 

		console.log (email);
		console.log (firstName);
		console.log (lastName);
		console.log (age);
		console.log (gender);
		console.log (mobileNo);
		console.log (password1);
		console.log (password2);


		// function to simulate user registration
		function registerUser(e){
			e.preventDefault()
			
			fetch('http://localhost:4000/api/users/register', {
				method: 'POST',
				headers:{
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					firstName:firstName,
					lastName:lastName,
					age:age,
					gender:gender,
					mobileNo:mobileNo,
					password: password1
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if(data.email === true){
					retrieveUserDetails(data.email)
						if(data.email !== retrieveUserDetails.setUser.data){
								Swal.fire({
									title: "Duplicate Email",
									icon: "error",
									text: "Please use a different email address."
								})
							}
					}else{
						Swal.fire({
							title: "Successfully registered",
							icon: "success",
							text: "You have successfully registered an account."
						})
						setFirstName('');
						setLastName('');
						setAge('');
						setGender('');
						setMobileNo('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						history.push("/login")
					}
			})

			// clears input fields after hitting submit button
			setEmail('')
			setFirstName('')
			setLastName('')
			setAge('')
			setGender('')
			setMobileNo('')
			setPassword1('')
			setPassword2('')

			console.log('successfully registered!')
		}

			const retrieveUserDetails = (token)=> {
				fetch('http://localhost:4000/api/users/checkEmail',{
					headers: {
						Authorization: `Bearer ${ token}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					setUser({
						email: data.email
					})
				})
			}

			// sets the email of the authenticated user in the local storage
			/*
			SYNTAX:
			localStorage.setItem('propertyName', value);
			*/
			// localStorage.setItem('email', email);
			
			// set the global user state to have properties obtained from the local storage
			/*setUser({
				email: localStorage.getItem('email')
			});*/


		useEffect(()=>{
			// validation to enable the submit button when all fields are populated and both passwords match
			if((email !== '' && firstName !== '' && lastName !== '' && age !== '' && gender !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '' ) && (password1 === password2)){
				setIsActive(true)
			}else{
				setIsActive(false)
			}
		}, [email, firstName, lastName, age, gender, mobileNo, password1, password2])
		return(
			(user.id !== null) ?
				<Redirect to='/login' />
			:
			<Container>
				<h1>Register</h1>
				<Form onSubmit={(e) => registerUser(e)}>

				  <Form.Group className="mb-3" controlId="firstName" required>
				    <Form.Label>First Name</Form.Label>
				    <Form.Control 
				    		type="firstName" 
				    		placeholder="First Name"
				    		value = {firstName}
				    		onChange= { e=> setFirstName(e.target.value) } 
				    		/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="lastName" required>
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control 
				    		type="lastName" 
				    		placeholder="Last Name"
				    		value = {lastName}
				    		onChange= { e=> setLastName(e.target.value) } 
				    		/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="age" required>
				    <Form.Label>Age</Form.Label>
				    <Form.Control 
				    		type="age" 
				    		placeholder="Age"
				    		value = {age}
				    		onChange= { e=> setAge(e.target.value) } 
				    		/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="gender" required>
				    <Form.Label>Gender</Form.Label>
				    <Form.Control 
				    		type="gender" 
				    		placeholder="Gender"
				    		value = {gender}
				    		onChange= { e=> setGender(e.target.value) } 
				    		/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="mobileNo" required>
				    <Form.Label>Mobile No.</Form.Label>
				    <Form.Control 
				    		type="mobileNo" 
				    		placeholder="09xxxxxxxxx"
				    		value = {mobileNo}
				    		onChange= { e=> setMobileNo(e.target.value) } 
				    		/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="userEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    		type="email" 
				    		placeholder="Enter email" 
				    		value = {email}
				    		onChange= { e=> setEmail(e.target.value) }
				    		required 
				    		/>
				    <Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="password1" required>
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    		type="password" 
				    		placeholder="Password"
				    		value = {password1}
				    		onChange= { e=> setPassword1(e.target.value) } 
				    		/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="password2" required>
				    <Form.Label>Verify Password</Form.Label>
				    <Form.Control 
				    		type="password" 
				    		placeholder="Verify Password" 
				    		value = {password2}
				    		onChange= { e=> setPassword2(e.target.value) } 
				    		required />
				  </Form.Group>
				{/*conditionally render submit button based on isActive state*/}
				  { isActive ?
						  <Button variant="primary" type="submit" id="submitBtn">
						    Submit
						  </Button>
						:
						  <Button variant="primary" type="submit" id="submitBtn" disabled>
						    Submit
						  </Button>
				  }
				</Form>
			</Container>
		)
}