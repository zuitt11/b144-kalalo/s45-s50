import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props) {
	// Allows consumption of UserContext object and its properties to use  for user validation
	const {user, setUser} = useContext(UserContext);

	//  state hooks to store the values of the input fields
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	// state to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false)

	// checks if the values are successfully binded 

	console.log (email);
	console.log (password);

	// function to sumulate user registration
	function loginUser(e){
		e.preventDefault()

		// Process a fetch request to the corresponding backend API
		/*
		SYNTAX
			fetch('url', {options})
			.then(res=>res.json())
			.then(data =>{})
		*/
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.accessToken)
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
				Swal.fire({
					title:"Logged In Successfully",
					icon: "success",
					text: "Welcome to Zuitt"
				})
			}else{
				Swal.fire({
					title:"Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})

		// sets the email of the authenticated user in the local storage
		/*
		SYNTAX:
		localStorage.setItem('propertyName', value);
		*/
		// localStorage.setItem('email', email);
		
		// set the global user state to have properties obtained from the local storage
		/*setUser({
			email: localStorage.getItem('email')
		});*/



		// clears input fields after hitting submit button
		setEmail('')
		setPassword('')

		console.log(`${email} has been verified! Welcome back!`);
	}

	const retrieveUserDetails = (token)=> {
		fetch('http://localhost:4000/api/users/details',{
			headers: {
				Authorization: `Bearer ${ token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		// validation to enable the submit button when all fields are populated and both passwords match
		if((email !== '' && password !== '' )){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])
	return(
		/*(user.email !== null) ?
			<Redirect to='/courses' />
		:*/
		<Container>
			<h1>Login</h1>
			<Form onSubmit={(e) => loginUser(e)}>
			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    		type="email" 
			    		placeholder="Enter email" 
			    		value = {email}
			    		onChange= { e=> setEmail(e.target.value) }
			    		required 
			    		/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password" required>
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    		type="password" 
			    		placeholder="Password"
			    		value = {password}
			    		onChange= { e=> setPassword(e.target.value) } 
			    		/>
			  </Form.Group>
			{/*conditionally render submit button based on isActive state*/}
			  { isActive ?
					  <Button variant="success" type="submit" id="submitBtn">
					    Login
					  </Button>
					:
					  <Button variant="success" type="submit" id="submitBtn" disabled>
					    Login
					  </Button>
			  }
			</Form>
		</Container>
	)
}